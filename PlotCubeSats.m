clear
close all

testType = 'Simulation_4_15_085';

%matFileDir = 'Data/Results/matFiles/';
matFileDir = 'newData/Results/';

matFileDirModular = [matFileDir testType '/data/'];
matFileDirSimulation = [matFileDir testType '/data/'];
matFileDir100m = [matFileDir testType '/data/'];

% switch testType
%     case 'ModularTest_4_9'
%         matFiles = dir([matFileDirModular 'CubeSatArrayTest*']);
%         matFileDir = matFileDirModular;
%     case '100m'
%         matFiles = dir([matFileDir100m 'CubeSatArrayTest*']);
%         matFileDir = matFileDir100m;
%     case 'Simulation_4_15_195'
%         matFiles = dir([matFileDirSimulation 'CubeSatArraySample*']);
%         %matFiles = dir([matFileDirSimulation 'CSDataSample*']);
%         matFileDir = matFileDirSimulation;
% end

%matFiles = dir([matFileDirSimulation 'CubeSatArraySample*']);
matFiles = dir([matFileDirSimulation 'CSDataSample*']);
matFileDir = matFileDirSimulation;

mkdir('FigureOut');
for i = 1:numel(matFiles)
    load([matFileDir matFiles(i).name]);
    %CubeSat = CubeSats(1);
    centroids = CubeSat.centroids_VCF;
    
    truthDir = strcat('Data/Simulation/',testType,'/truth.json');
    [x,y,z] = truthPlot(truthDir);
    
    for j = 1:numel(centroids)
        X(j) = centroids{j}(1);
        Y(j) = centroids{j}(2);
        Z(j) = centroids{j}(3);
    end
    tmp = split(matFiles(i).name,'.');
    filename = tmp{1};
    tmp = split(filename,'CubeSatArray');
    testNum = tmp{2};
    figure
    hold on
    plot3(X,Y,Z)
    plot3(x,y,z)
    %savefig(['FigureOut/' testNum testType])
end

function [x,y,z] = truthPlot(truthDir)
    truthData = jsondecode(fileread(truthDir));
    for k = 1:length(truthData)
        x(k) = truthData(k).pos.launch_num_2__1U_Slug_SLDPRT(1);
        y(k) = truthData(k).pos.launch_num_2__1U_Slug_SLDPRT(2);
        z(k) = truthData(k).pos.launch_num_2__1U_Slug_SLDPRT(3);
    end
end
