configfile = [pwd '/config/Simulation/_085/Sample1'];
% configfile = [pwd '/config/ModularTest_4_9/Test5'];
% configfile = [pwd '/config/Simulation/_1U1U3U/Sample1'];
caseName = '085';

%%% Filenames and configurables
manifestFilename = strcat(configfile,'/Manifest.json');
SensorData = jsondecode(fileread(strcat(configfile,'/Sensors.json')));

% Develop model
Model = VISION.PostProcessing.Model(manifestFilename,configfile,true);

fileLims = [1 inf];
% Update model with ToF centroids
Model.Deployer = Model.TOF.TOFProcessing(SensorData,...
    Model.Deployer,'presentResults',0,'fileLims',fileLims,'showDebugPlots',0);

% Process truth data
Truth = Model.Truth_VCF;

%% CDR sensor uncertainty
% print raw tof centroids
CubeSat = Model.Deployer.CubesatArray;
centroids_tof = cell(6,1);
centroids_tof_time = cell(6,1);

% get all 6 centroids before combined with optical
for a = 1:length(CubeSat)
    centroids_tof{a} = CubeSat(a).centroids_VCF';
    centroids_tof_time{a} = CubeSat(a).time';
end

% get position and time
centroid_calc.pos = centroids_tof;
centroid_calc.time = centroids_tof_time;
centroid_truth.time = Truth.t;
centroid_truth.pos = Truth.Cubesat;
  
% save data
dataFolder = strcat('newData/Results/Simulation_4_15_',caseName,'/');
tmp = split(SensorData.TOFData,'/');
testNumber = tmp{5};
mkdir(dataFolder)
mkdir([dataFolder 'data/']);
save([pwd '/' dataFolder 'data/centroid_VCF_calc' testNumber '.mat'],'centroid_calc');
save([pwd '/' dataFolder 'data/centroid_VCF_truth' testNumber '.mat'],'centroid_truth');


%% Compute results
Model.ComputeStateOutput();

% Sensor fusion
Validator = Validate(configfile,Model,true);

% Initialize 
CubeSatFitted = cell(Model.Deployer.numCubesats,1);
TruthFitted = cell(Model.Deployer.numCubesats,1);
AbsoluteError = cell(Model.Deployer.numCubesats,1);
XError = cell(Model.Deployer.numCubesats,1);
YError = cell(Model.Deployer.numCubesats,1);
ZError = cell(Model.Deployer.numCubesats,1);

data_t = Model.Deployer.CubesatArray(1).time(end);
t_fit = linspace(0,data_t);

% Update Model and extrapolate
for i=1:Model.Deployer.numCubesats
   CubeSat = Model.Deployer.CubesatArray(i);
   CubeSatFitted{i} = Validator.fitCubeSatTraj(CubeSat.centroids_VCF,CubeSat.time,'CS',t_fit,Model);
   TruthFitted{i} = interp1(Model.Truth_VCF.t,Model.Truth_VCF.Cubesat(i).pos,t_fit,'linear','extrap');
   [AbsoluteError{i},XError{i},YError{i},ZError{i}] = ...
       Validator.ProcessError(CubeSatFitted{i},TruthFitted{i});
end

% dataFolder = strcat('newData/Results/Simulation_4_15_',caseName,'/');
% %dataFolder = strcat('newData/Results/ModularTest_4_9/Test5/');
% tmp = split(SensorData.TOFData,'/');
% testNumber = tmp{5};
% mkdir(dataFolder)
% mkdir([dataFolder 'data/']);
save([pwd '/' dataFolder 'data/CSData' testNumber '.mat'],'CubeSatFitted');
save([pwd '/' dataFolder 'data/TruthData' testNumber '.mat'],'TruthFitted');
save([pwd '/' dataFolder 'data/AbsErrorData' testNumber '.mat'],'AbsoluteError');
save([pwd '/' dataFolder 'data/XErrorData' testNumber '.mat'],'XError');
save([pwd '/' dataFolder 'data/YErrorData' testNumber '.mat'],'YError');
save([pwd '/' dataFolder 'data/ZErrorData' testNumber '.mat'],'ZError');
save([pwd '/' dataFolder 'data/CSTime' testNumber '.mat'],'t_fit');
centroids = CubeSat.centroids_VCF;
save([pwd '/' dataFolder 'data/centroid_VCF' testNumber '.mat'],'centroids');
save([pwd '/' dataFolder 'data/CubeSatArray' testNumber '.mat'],'CubeSat');

% configDirecName = strcat('config/Simulation/_',caseName,'/Sample*');
% %testType = 'Simulation/_195';
% testDef = strcat('Sim',caseName);
% 
% configfiles = dir(configDirecName);
% 
% Validator.ErrorAnalysis(Model,SensorData,testDef);
% 
% matFileDirectory = [pwd '/newData/Results/matFiles'];
% Validator.GenerateOutputFiles(matFileDirectory);