clc
close all
clear

%% File Definitions
% High-level directory of all TOF information. This should contain the
% executable data capture script and the desired location to save point
% cloud outputs.
% tofDirName = 'Data/Simulation/Simulation_4_15_140/TOF/';
% tofDirPointClouds = strcat(tofDirName,'Sample1/');
% tofDirName = 'test_files/';
% tofDirPointClouds = strcat(tofDirName,'TRR_example/');
% tofDirPointClouds = strcat(tofDirName,'CleanTOF_1U1U3U/');
tofDirName = 'Data/ModularTest_4_9/';
tofDirPointClouds = strcat(tofDirName,'Test8/TOF/');
% tofDirPointClouds = strcat(tofDirName,'100m_Tube3_Test1_TOF/');

% Grab all point cloud files in the defined output directory.
% PointClouds = dir([tofDirPointClouds 'VTube*']);
PointClouds = dir([tofDirPointClouds '*']);

%% TOF Plotting Parameters
% Azimuth, Elevation - These parameters automatically adjust the view of
% the generated plots to be looking outwards, as if you are the TOF camera.
% For this to be accurate, have the TOF take images upright (The LED lights
% on the back, 2 green, 2 orange, should on top).
az = 90;
el = 180;


%% Plot Point Clouds
% This for loop iterates each detected point cloud in the defined directory
% above, and sequentially generates each in a single figure.
for ii = 1:length(PointClouds)
    
    fname = PointClouds(ii).name;
    isDefFName = strcmp(fname, '.') || strcmp(fname, '..');
    
    if ~isDefFName
        fpath = fullfile(tofDirPointClouds, fname);
        testFile = pcread(fpath);
        
        pcshow(testFile)
        xlabel('x [mm]')
        ylabel('y [mm]')
        zlabel('z [mm]')
        colormap('parula')
        view(az,el);
        drawnow
    end
end