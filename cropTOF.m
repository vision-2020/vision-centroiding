%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Written by: Adam Boylston %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
close all
clear

%% File Definitions
% High-level directory of all TOF information. This should contain the
% executable data capture script and the desired location to save point
% cloud outputs.
% tofDirName = '4_Simulation_Cases/blensor_test1/ToF_Data/';
tofDirName = 'test_files/';
tofDirPointClouds = strcat(tofDirName,'Config_085/Sample1/');

% Grab all point cloud files in the defined output directory.
PointClouds = dir([tofDirPointClouds 'Sample*']);

%% TOF Plotting Parameters
% Azimuth, Elevation - These parameters automatically adjust the view of
% the generated plots to be looking outwards, as if you are the TOF camera.
% For this to be accurate, have the TOF take images upright (The LED lights
% on the back, 2 green, 2 orange, should on top).
% az = 0;
% el = 270-180;
az = 90;
el = 180;
plt = 0; % only set plt = 1 if stepping through or if only running through a few frames

newFolder = 'test_files/CleanConfig_085/';
mkdir(newFolder) 
        
%% Plot Point Clouds
% This for loop iterates each detected point cloud in the defined directory
% above, and sequentially generates each in a single figure.
for ii = 1:length(PointClouds)
    disp(ii)
    fname = PointClouds(ii).name;
    isDefFName = strcmp(fname, '.') || strcmp(fname, '..');
    
    if ~isDefFName
        fpath = fullfile(tofDirPointClouds, fname);
        testFile = pcread(fpath); % loaded PCD
        
        % remove background at max distance 
        zmax = 0.9*max(max(testFile.Location(:,:,3)));
        cleanInds = double(testFile.Location(:,:,3) < zmax);
        cleanInds(cleanInds==0) = NaN;
        dirtyPoints = testFile.Location;
        cleanPoints = dirtyPoints.*cleanInds;
        testFileClean = pointCloud(cleanPoints);
        testFileClean = removeInvalidPoints(testFileClean);
    
        if ~isempty(testFileClean.Location)
            fpathClean = strcat(newFolder,'Clean',fname);
            pcwrite(testFileClean,fpathClean);
            cleanPC = pcread(fpathClean);

            if plt
                % plot old and new point clouds
                figure
                subplot(1,2,1)
                pcshow(testFile)
                title('Original Point Cloud')
                colormap('parula')
                view(az,el);
                xlabel('x')
                ylabel('y')
                zlabel('z')
                drawnow

                subplot(1,2,2)
                %pcshow(testFileClean)
                pcshow(cleanPC)
                title('Cropped Point Cloud')
                colormap('parula')
                view(az,el);
                xlabel('x')
                ylabel('y')
                zlabel('z')
                drawnow
            end
        end
    end
end

%% verification

% fig = gcf; % needs to be original figure
% dataObjs = findobj(fig,'-property','YData');
% x = dataObjs.XData;
% y = dataObjs.YData;
% z = dataObjs.ZData;
% 
% tooBigInd = find(z < 0.90*max(z));
% newx = x(tooBigInd);
% newy = y(tooBigInd);
% newz = z(tooBigInd);
% 
% figure
% subplot(1,2,1)
% plot3(x,y,z,'.')
% % axis([-.5 .5 -.5 .5
% zlabel('z')
% subplot(1,2,2)
% plot3(newx,newy,newz,'.')



