# General Comments:
  - VISION used most for VANTAGE's centroid determination suite, there was little modification from VANTAGE.
  - There is one example of simulation data here since we never got any experimental data.
  - This folder has all the original scripts for the centroiding suites, which is later modified for the state estimation simulations.


## Here is the directory:

```bash
├── +VISION
│   └── +PostProcessing
│       ├── CubeSat_Optical.m
│       ├── CubeSat_TOF.m
│       ├── CubeSat.m
│       ├── Deployer.m
│       ├── Model.m
│       ├── Optical.m
│       ├── TimeManager.m
│       ├── TOF.m
│       └── Transform.m
├── config
│   └── Simulation
│       │   └── _085
│       │       ├── Sample1
│       │       │    ├── Deployer.json
│       │       │    ├── Manifest.json
│       │       │    ├── Optical.json
│       │       │    ├── Sensors.json
│       │       │    ├── TOF.json
│       │       │    ├── Transform.json
│       │       │    └── Validate.json
│       │       ├── Sample2
│       │       │    └── ...
├── Data
│   └── Simulation
│       │   └── Simulation_4_15_085
│       │       ├── Optical
│       │       │    ├── VTube4_DTube3_CubeSatsSix1U_Speed085cmps_Camera_a0000.png
│       │       │    └── ...
│       │       ├── TOF
│       │       │    ├── Sample1
│       │       │    │    ├── VTube4_DTube3_CubeSatsSix1U_Speed085cmps-04_14_48_2019_04_15_noisy00001.pcd
│       │       │    │    └── ...
│       │       │    ├── Sample2
│       │       │    │    ├── VTube4_DTube3_CubeSatsSix1U_Speed085cmps-04_14_48_2019_04_15_noisy00001.pcd
│       │       │    │    └── ...
│       │       ├── truth_corrected.json
│       │       ├── truth_corrections.json
│       │       └── truth.json
├── cropTOF.m
├── PlotCubeSats.m
├── PlotTOFPointClouds.m
├── README.md
├── Validate.m
└── VantageMain.m
```


1. +VISION/+PostProcessing contains all the MATLAB scripts
2. config folder has all the .json files that initializes parameters such as deployer, sensors, verification factors, etc, for each sample.
2. Data folder has all the sensor data and truth file
3. cropTOF.m is just a script that manually crops the TOF when we had problems, but it shouldn't give you any problem if you remove the cube in BlenSor.
4. PlotCubeSats.m and PlotTOFPointCloud.m just plots the processed data and raw data, respectively.
5. Validate.m just needs to be there and VantageMain.m is the main script that runs all the post processing code. 
